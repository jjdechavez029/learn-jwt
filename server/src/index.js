require('dotenv/config');
const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const { verify } = require('jsonwebtoken');
const { hash, compare } = require('bcryptjs');
const { createAccessToken, createRefreshToken, sendAccessToken, sendRefreshToken } = require('./tokens');

const { fakeDB } = require('./fakeDB');
const { isAuth } = require('./isAuth');

// Register
// Login
// Logout
// Setup protected route
// Get new accesstoken with refresh token

const server = express();

// middleware cookie handling
server.use(cookieParser());
server.use(
    cors({
        origin: 'http://localhost:3000', // Client
        credentials: true // client & server can comunicate
    })
);

// Needed body data
server.use(express.json()); // json-encoded bodies
server.use(express.urlencoded({ extended: true })); // url-encoded

// register
server.post('/register', async (req, res) => {
    const { email, password } = req.body;

    try {
        // check user exist
        const user = fakeDB.find(user => user.email === email);
        if (user) throw new Error('User already exist');
        // user not exist, hash the password
        const hashedPassword = await hash(password, 10); // 10 means 10 times sorted
        // insert the user in 'database'
        fakeDB.push({
            id: fakeDB.length + 1,
            email,
            password: hashedPassword
        })
        res.send({ message: 'User Created' });
        console.log(fakeDB);
    } catch (error) {
        res.send({
            error: `${error.message}`
        })
    }
});

// login user
server.post('/login', async (req, res) => {
    const { email, password } = req.body;

    try {
        // find the user exist
        const user = fakeDB.find(user => user.email === email);
        if (!user) throw new Error("User does not exist");
        // compare password
        const valid = await compare(password, user.password);
        if (!valid) throw new Error("Password not correct");
        
        // Create refresh(long lifetime) and accesstoken(short lifetime)
        const accesstoken = createAccessToken(user.id);
        const refreshtoken = createRefreshToken(user.id);
        // put the refreshtoken in db
        user.refreshtoken = refreshtoken;
        console.log(fakeDB);
        // send token, refreshtoken as a cookie and accesstoken as a regular response
        sendRefreshToken(res, refreshtoken);
        sendAccessToken(res, req, accesstoken);
    } catch (error) {
        res.send({
            error: `${error.message}`
        })
    }
});

// logout
server.post('/logout', (req, res) => {
    res.clearCookie('refreshtoken', { path: '/refresh_token' }); // wipeout refreshtoken to cookie 
    return res.send({ message: 'Logged out '});
});

// Protected route
server.post('/protected', async (req, res) => {
    try {
        const userId = isAuth(req);
        if (userId !== null) {
            res.send({
                data: 'This is protected data.'
            })
        }
    } catch (error) {
        res.send({ error: `${error.message}`});
    }
});

// get new accesstoken with refresh token
server.post('/refresh_token', (req, res) => {
    const token = req.cookies.refreshtoken;
    // don't have token on req
    if (!token) return res.send({ accesstoken: '' });
    // if token available, then verify the token
    let payload = null;

    try {
        payload = verify(token, process.env.REFRESH_TOKEN_SECRET);
    } catch (error) {
        return res.send({ accesstoken: ''});
    }

    // token is valid, check if the user exist
    const user = fakeDB.find(user => user.id === payload.userId); // payload.userId came from on creating refreshtoken
    if (!user) return res.send({ accesstoken: '' });
    // user exist, check if refreshtoken exist on user
    if (user.refreshtoken !== token) {
        return res.send({ accesstoken: '' });
    }

    // token exist, create new refresh and access token
    const accesstoken = createAccessToken(user.id);
    const refreshtoken = createRefreshToken(user.id);

    user.refreshtoken = refreshtoken;
    // send new refresh and access token
    sendRefreshToken(res, refreshtoken);
    console.log(user);
    res.send({ accesstoken });
});

server.listen(process.env.PORT, () => console.log(`Server listening on port ${process.env.PORT}`));